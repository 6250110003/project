import 'package:flutter/material.dart';

class About extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('About'),
           backgroundColor: Colors.pink,
        ),
        body: Container(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                      width: 400.0,
                      height: 250.0,
                      alignment: Alignment.center,
                      child: Image.asset('6250110003.jpg'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      'นางสาวนภัสสร บูก้ง 6250110003 ICM',
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                  Container(
                    width: 400.0,
                    height: 250.0,
                    alignment: Alignment.center,
                    child: Image.asset('6250110026.jpg'),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      'นางสาวนุชนาถ เหมาะแหล่ 6250110026 ICM',
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                ],
              ),
            )));
  }
}
